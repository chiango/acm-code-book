#include <cstdio>
#include <cctype>

int nextInt() {
	int c, ret = 0;
	while(!isdigit(c = getchar())) ;
	do {
		ret = (ret << 3) + (ret << 1) + c - '0';
		c = getchar();
	} while(isdigit(c));
	return ret;
}

typedef long long ll;
ll nextll() {
	int c; ll ret = 0;
	while(!isdigit(c = getchar())) ;
	do {
		ret = (ret << 3) + (ret << 1) + c - '0';
		c = getchar();
	} while(isdigit(c));
	return ret;
}

int main() {
	int n; scanf("%d", &n);
	while(n--) printf("%lld\n", nextll());
	return 0;
}