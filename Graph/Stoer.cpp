#include <cstdio>
#include <cstring>

const int maxn = 510, inf = 0x7fffffff;
int n, m, g[maxn][maxn];

int stoer_wagner(int n) {
	int ret = 1000000;
	int dis[maxn], v[maxn];
	bool vis[maxn];
	for(int i = 0; i < n; ++i) {
		v[i] = i;
	}
	while(n > 1) {
		int now = 1, pre = 0;
		for(int i = 1; i < n; ++i) {
			dis[v[i]] = g[v[0]][v[i]];
			if(dis[v[i]] > dis[v[now]])
				now = i;
		}
		memset(vis, 0, sizeof vis);
		vis[v[0]] = true;
		for(int i = 1; i < n; ++i) {
			if(i == n-1) {
				if(dis[v[now]] < ret) ret = dis[v[now]];
				for(int j = 0; j < n; ++j) {
					g[v[pre]][v[j]] += g[v[now]][v[j]];
					g[v[j]][v[pre]] += g[v[now]][v[j]];
				}
				v[now] = v[--n];
			}
			else {
				vis[v[now]] = true;
				pre = now; now = -1;
				for(int i = 1; i < n; ++i)
					if(!vis[v[i]]) {
						dis[v[i]] += g[v[pre]][v[i]];
						if(now == -1 || dis[v[i]] > dis[v[now]])
							now = i;
					}
			}
		}
	}
	return ret;
}