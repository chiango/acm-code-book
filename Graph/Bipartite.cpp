int odd[maxn], color[maxn];
bool bipartite(int u, int b) {
  for(int i = 0; i < G[u].size(); i++) {
    int v = G[u][i]; if(bccno[v] != b) continue;
    if(color[v] == color[u]) return false;
    if(!color[v]) {
      color[v] = 3 - color[u];
      if(!bipartite(v, b)) return false;
    }
  }
  return true;
