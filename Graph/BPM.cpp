#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>
using namespace std;

const int maxn = 500 + 5; // `单侧顶点的最大数目`

// `二分图最大基数匹配，邻接矩阵写法`
struct BPM {
  int n, m;               // `左右顶点个数`
  int G[maxn][maxn];      // `邻接表`
  int left[maxn];         // `left[i]为右边第i个点的匹配点编号，-1表示不存在`
  bool T[maxn];           // `T[i]为右边第i个点是否已标记`

  void init(int n, int m) {
    this->n = n;
    this->m = m;
    memset(G, 0, sizeof(G));
  }

  bool match(int u){
    for(int v = 0; v < m; v++) if(G[u][v] && !T[v]) {
      T[v] = true;
      if (left[v] == -1 || match(left[v])){
        left[v] = u;
        return true;
      }
    }
    return false;
  }

  // `求最大匹配`
  int solve() {
    memset(left, -1, sizeof(left));
    int ans = 0;
    for(int u = 0; u < n; u++) { // `从左边结点u开始增广`
      memset(T, 0, sizeof(T));
      if(match(u)) ans++;
    }
    return ans;
  }

};
