#include<cstdio>
#include<algorithm>
using namespace std;

const int maxn = 100000 + 10;
const int logmaxn = 20;
const int INF = 1000000000;

struct LCA {
  int n;
  int fa[maxn];   // `父亲数组`
  int cost[maxn]; // `和父亲的费用`
  int L[maxn];    // `层次（根节点层次为0）`
  int anc[maxn][logmaxn];     // `anc[p][i]是结点p的第`2^i级`父亲。anc[i][0] = fa[i]`
  int maxcost[maxn][logmaxn]; // `maxcost[p][i]是i和anc[p][i]的路径上的最大费用`

  // `预处理，根据fa和cost数组求出anc和maxcost数组`
  void preprocess() {
    for(int i = 0; i < n; i++) {
      anc[i][0] = fa[i]; maxcost[i][0] = cost[i];
      for(int j = 1; (1 << j) < n; j++) anc[i][j] = -1;
    }   
    for(int j = 1; (1 << j) < n; j++)
      for(int i = 0; i < n; i++)
        if(anc[i][j-1] != -1) {
          int a = anc[i][j-1];
          anc[i][j] = anc[a][j-1];
          maxcost[i][j] = max(maxcost[i][j-1], maxcost[a][j-1]);
        }
  }

  // `求p到q的路径上的最大权`
  int query(int p, int q) {
    int tmp, log, i;
    if(L[p] < L[q]) swap(p, q);
    for(log = 1; (1 << log) <= L[p]; log++); log--;

    int ans = -INF;
    for(int i = log; i >= 0; i--)
      if (L[p] - (1 << i) >= L[q]) { ans = max(ans, maxcost[p][i]); p = anc[p][i];}
   
    if (p == q) return ans; // `LCA为p`
   
    for(int i = log; i >= 0; i--)
      if(anc[p][i] != -1 && anc[p][i] != anc[q][i]) {
        ans = max(ans, maxcost[p][i]); p = anc[p][i];
        ans = max(ans, maxcost[q][i]); q = anc[q][i];
      }

    ans = max(ans, cost[p]);
    ans = max(ans, cost[q]);
    return ans; // `LCA为fa[p]（它也等于fa[q]）`
  }  
};

LCA solver;
