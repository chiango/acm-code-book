void DFS(int cur,int par)
 {
     dfn[cur]=low[cur]=++Index;
      
     int size=adj[cur].size();
     int cnt=0;
     for(int i=0;i<size;i++)
     {    
         int v=adj[cur][i];
         if(!dfn[v])
         {
             cnt++;
             DFS(v,cur);
             if(low[cur]>low[v])
                 low[cur]=low[v];
             if((cur==root&&cnt==2)||(cur!=root&&low[v]>=dfn[cur]))
                 flag[cur]=true;
         }
         else if(v!=par&&low[cur]>dfn[v])
             low[cur]=dfn[v];
     }
 }

void CutEdge(int cur,int par)
 {    dfn[cur]=low[cur]=++Index;
      
      for(int i=head[cur];i;i=buf[i].next)
     {
         int v=buf[i].v;
         if(v==par)continue;
         if(!dfn[v])
         {
             CutEdge(v,cur);
             if(low[cur]>low[v])
                 low[cur]=low[v];
             if(low[v]>dfn[cur])
             {    
                     ans[nAns++]=buf[i].id;
             }
         }
         else if(low[cur]>dfn[v])
             low[cur]=dfn[v];
     }
 }

