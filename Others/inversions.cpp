const int maxn = 80;
int _tmp[maxn];
int inversions(int n, int *a) {
	int l = n>>1, r = n-l, i, j;
	int ret = (r>1?(inversions(l, a) + inversions(r, a+l)) : 0);
	for(i = j = 0; i <= l; _tmp[i+j] = a[i], ++i)
		for(ret += j; j < r && ( i == l || a[i] > a[l+j]); _tmp[i+j] = a[l+j], ++j);
	memcpy(a, _tmp, sizeof(int) * n);
	return ret;
}
