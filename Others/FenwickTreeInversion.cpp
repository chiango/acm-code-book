#include <vector>
#include <cstdio>
#include <cstring>
using namespace std;
inline int lowbit(int x) { return x&-x; }
struct FenwickTree {
	int n;
	vector<int> C;

	void resize(int n) { this->n = n; C.resize(n); }
	void clear() { fill(C.begin(), C.end(), 0); }

	int sum(int x) {
		int ret = 0;
		while(x > 0) {
			ret += C[x]; x -= lowbit(x);
		}
		return ret;
	}

	void add(int x, int d) {
		while(x <= n) {
			C[x] += d; x += lowbit(x);
		}
	}
};

FenwickTree tree;
const int maxn = 60;
int n, arr[maxn];
int main() {
	tree.resize(60);
	int t; scanf("%d", &t);
	while(t--) {
		tree.clear();
		scanf("%d", &n);
		int ans = 0;
		for(int i = 1; i <= n; ++i) {
			scanf("%d", &arr[i]);
			ans += i - tree.sum(arr[i]) - 1; 
			tree.add(arr[i], 1);
		}
		printf("Optimal train swapping takes %d swaps.\n", ans);
	}
	return 0;
}


