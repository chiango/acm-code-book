//100203B - Bracelets
#include <cstdio>
#include <cstring>
#include <iostream>

const int up = 2, diag = 1, left = 0;

int d[4000][4000], p[4000][4000] = {{0}};
void LCS(char *A, char *B, int m, int n) {

	for(int i = 1; i <= m; ++i)
		for(int j = 1; j <= n; ++j) {
			if(A[i] == B[j] && d[i-1][j-1]+1 > d[i][j-1]) {
				d[i][j] = d[i-1][j-1] + 1;
				p[i][j] = diag;
			}
			else if(d[i][j-1] >= d[i-1][j]) {
				d[i][j] = d[i][j-1];
				p[i][j] = left;
			}
			else {
				d[i][j] = d[i-1][j];
				p[i][j] = up;
			}
		}
}

int Trace(int m, int n) {
	int ret = 0;
	while(m > 0 && n > 0) {
		if(p[m][n] == left) n--;
		else if(p[m][n] == up) m--;
		else { n--; m--; ret++; }
	}
	return ret;
}

void Reroot(int root, int m, int n) {
	int i = root, j = 1;
	while(j <= n && p[i][j] != diag) {
		j++;
	}
	if(j > n) return;
	p[i][j] = left;
	while(i < 2*m && j < n) {
		if(p[i+1][j] == up) {
			i++; p[i][j] = left;
		}
		else if(p[i+1][j+1] == diag) {
			i++; j++;
			p[i][j] = left;
		}
		else j++;
	}
	while(i < 2*m && p[i+1][j] == up) {
		i++;
		p[i][j] = left;
	}
}

int CLCS(char *A, char *B) {
//	printf("CLCS %s %s = ", A+1, B+1);
	int m = strlen(A+1);
	int n = strlen(B+1);

	memset(d, 0, sizeof d);

	char AA[4000];
	for(int i = 1; i <= m; ++i)
		AA[i] = AA[i+m] = A[i];

	LCS(AA, B, m*2, n);
	int S = Trace(m, n);
	for(int i = 1; i < m; ++i) {
		Reroot(i, m, n);
		int Sp = Trace(m+i, n);
		if(Sp > S) S = Sp;
	}
	return S;
}

char A[2000], B[2000];
int main() {
	scanf("%s%s", A+1, B+1);
	int ans = CLCS(A, B);

	int m = strlen(B+1); int n = m/2;
	for(int i = 1; i <= n; ++i)
		std::swap(B[i], B[m-i+1]);

	ans = std::max(ans, CLCS(A,B));
	printf("%d\n", ans*2);
	return 0;
}
