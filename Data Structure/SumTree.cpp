#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
const int maxn = 100010;

int ql, qr, d;
struct Tree {
	int node[maxn*4], delta[maxn*4];

	void clear(int n) {
		memset(node, 0, sizeof(int)*n);
		memset(delta, 0, sizeof (int)*n);
	}

	inline void maintain(int o, int l, int r) {
		int lc = o*2, rc = o*2 + 1;
		node[o] = 0;
		if(r > l) node[o] = max(node[lc], node[rc]);
		node[o] += delta[o];
	}

	void add(int o, int l, int r) {
		int lc = o*2, rc = o*2 + 1;
		if(ql <= l && r <= qr) {
			delta[o] += d;
		}
		else {
			int m = (l+r)>>1;
			if(ql <= m) add(lc, l, m);
			if(qr > m) add(rc, m+1, r);
		}
		maintain(o, l, r);
	}

	int ma;
	void query(int o, int l, int r, int ad) {
		if(ql <= l && r <= qr) {
			ma = max(ma, node[o] + ad);
		}
		else {
			int m = (l+r)>>1;
			if(ql <= m) query(o*2, l, m, ad + delta[o]);
			if(qr > m) query(o*2+1, m+1, r, ad+delta[o]);
		}
	}
} tree;