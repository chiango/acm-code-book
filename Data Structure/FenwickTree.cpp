inline int lowbit(int x) { return x&-x; }
struct FenwickTree {
	int n;
	vector<int> C;

	void resize(int n) { this->n = n; C.resize(n); }
	void clear() { fill(C.begin(), C.end(), 0); }

	int sum(int x) {
		int ret = 0;
		while(x > 0) {
			ret += C[x]; x -= lowbit(x);
		}
		return ret;
	}

	void add(int x, int d) {
		while(x <= n) {
			C[x] += d; x += lowbit(x);
		}
	}
};
