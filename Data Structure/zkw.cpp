#include <cstring>
#include <iostream>
using namespace std;

const int maxn = 100, maxnode = maxn * 3, inf = 2100000000;
struct ZKW {
	int n, c[maxnode];
	void init(int n) { memset(c, 0, sizeof c); this->n = n; }
	void add(int x, int d) {
		for(c[x += n] += d, x >>= 1; x; x>>= 1)
			c[x] = c[x+x] + c[x+x+1];
	}
	int sum(int s, int t) {
		int ret = 0;
		for(s = s+n-1, t = t+n+1; s^t^1; s>>=1, t>>=1) {
			if(!(s&1)) ret += c[s^1];
			if(t&1) ret += c[t^1];
		}
		return ret;
	}
	int Max(int s, int t) {
		int lmax = -inf, rmax = -inf;
		for(s = s+n-1, t = t+n+1; s^t^1; s>>=1, t>>=1) {
			if(!(s&1)) lmax = max(lmax, c[s^1]);
			if(t&1) rmax = max(rmax, c[t^1]);
		}
		return max(lmax, rmax);
	}
} zkw;

#include <cstdio>
int main() {
	zkw.init(30);
	zkw.add(1, 10);
	zkw.add(5, 20);
	printf("%d %d\n", zkw.sum(1, 3), zkw.sum(1, 8));
	printf("max = %d\n", zkw.Max(1,10));
	return 0;
}