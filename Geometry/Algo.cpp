// `点集凸包`
// `如果不希望在凸包的边上有输入点，把两个 <= 改成 <`
// `如果不介意点集被修改，可以改成传递引用`
vector<Point> ConvexHull(vector<Point> p) {
  // `预处理，删除重复点`
  sort(p.begin(), p.end());
  p.erase(unique(p.begin(), p.end()), p.end());

  int n = p.size();
  int m = 0;
  vector<Point> ch(n+1);
  for(int i = 0; i < n; i++) {
    while(m > 1 && Cross(ch[m-1]-ch[m-2], p[i]-ch[m-2]) <= 0) m--;
    ch[m++] = p[i];
  }
  int k = m;
  for(int i = n-2; i >= 0; i--) {
    while(m > k && Cross(ch[m-1]-ch[m-2], p[i]-ch[m-2]) <= 0) m--;
    ch[m++] = p[i];
  }
  if(n > 1) m--;
  ch.resize(m);
  return ch;
}

// `多边形的有向面积`
double PolygonArea(vector<Point> p) {
  double area = 0;
  int n = p.size();
  for(int i = 1; i < n-1; i++)
    area += Cross(p[i]-p[0], p[i+1]-p[0]);
  return area/2;
}

// `过两点p1, p2的直线一般方程ax+by+c=0`
// `(x2-x1)(y-y1) = (y2-y1)(x-x1)`
void getLineGeneralEquation(const Point& p1, const Point& p2, double& a, double& b, double &c) {
  a = p2.y-p1.y;
  b = p1.x-p2.x;
  c = -a*p1.x - b*p1.y;
}

int IsPointInPolygon(const Point& p, const vector<Point>& poly){
  int wn = 0;
  int n = poly.size();
  for(int i = 0; i < n; i++){
    const Point& p1 = poly[i];
    const Point& p2 = poly[(i+1)%n];
    if(p1 == p || p2 == p || OnSegment(p, p1, p2)) return -1; //` 在边界上`
    int k = dcmp(Cross(p2-p1, p-p1));
    int d1 = dcmp(p1.y - p.y);
    int d2 = dcmp(p2.y - p.y);
    if(k > 0 && d1 <= 0 && d2 > 0) wn++;
    if(k < 0 && d2 <= 0 && d1 > 0) wn--;
  }
  if (wn != 0) return 1; // `内部`
  return 0; // `外部`
}

//10256
bool ConvexPolygonDisjoint(const vector<Point> ch1, const vector<Point> ch2) {
  int c1 = ch1.size();
  int c2 = ch2.size();
  for(int i = 0; i < c1; i++)
    if(IsPointInPolygon(ch1[i], ch2) != 0) return false; // `内部或边界上`
  for(int i = 0; i < c2; i++)
    if(IsPointInPolygon(ch2[i], ch1) != 0) return false; // `内部或边界上`
  for(int i = 0; i < c1; i++)
    for(int j = 0; j < c2; j++)
      if(SegmentProperIntersection(ch1[i], ch1[(i+1)%c1], ch2[j], ch2[(j+1)%c2])) return false;
  return true;
}

// `返回点集直径的平方`
int diameter2(vector<Point>& points) {
  vector<Point> p = ConvexHull(points);
  int n = p.size();
  if(n == 1) return 0;
  if(n == 2) return Dist2(p[0], p[1]);
  p.push_back(p[0]); // `免得取模`
  int ans = 0;
  for(int u = 0, v = 1; u < n; u++) {
    // `一条直线贴住边p[u]-p[u+1]`
    for(;;) {
      // `当Area(p[u], p[u+1], p[v+1]) <= Area(p[u], p[u+1], p[v])时停止旋转`
      // `即Cross(p[u+1]-p[u], p[v+1]-p[u]) - Cross(p[u+1]-p[u], p[v]-p[u]) <= 0`
      // `根据Cross(A,B) - Cross(A,C) = Cross(A,B-C)`
      // `化简得Cross(p[u+1]-p[u], p[v+1]-p[v]) <= 0`
      int diff = Cross(p[u+1]-p[u], p[v+1]-p[v]);
      if(diff <= 0) {
        ans = max(ans, Dist2(p[u], p[v])); //` u和v是对踵点`
        if(diff == 0) ans = max(ans, Dist2(p[u], p[v+1])); // `diff == 0时u和v+1也是对踵点`
        break;
      }
      v = (v + 1) % n;
    }
  }
  return ans;
}


const double INF = 1e8;
const double eps = 1e-6;

// `半平面交主过程`
vector<Point> HalfplaneIntersection(vector<Line> L) {
  int n = L.size();
  sort(L.begin(), L.end()); // `按极角排序`
  
  int first, last;         // `双端队列的第一个元素和最后一个元素的下标`
  vector<Point> p(n);      // `p[i]为q[i]和q[i+1]的交点`
  vector<Line> q(n);       // `双端队列`
  vector<Point> ans;       // `结果`

  q[first=last=0] = L[0];  // `双端队列初始化为只有一个半平面L[0]`
  for(int i = 1; i < n; i++) {
    while(first < last && !OnLeft(L[i], p[last-1])) last--;
    while(first < last && !OnLeft(L[i], p[first])) first++;
    q[++last] = L[i];
    if(fabs(Cross(q[last].v, q[last-1].v)) < eps) { // `两向量平行且同向，取内侧的一个`
      last--;
      if(OnLeft(q[last], L[i].P)) q[last] = L[i];
    }
    if(first < last) p[last-1] = GetLineIntersection(q[last-1], q[last]);
  }
  while(first < last && !OnLeft(q[first], p[last-1])) last--; // `删除无用平面`
  if(last - first <= 1) return ans; // `空集`
  p[last] = GetLineIntersection(q[last], q[first]); //`计算首尾两个半平面的交点`

  // `从deque复制到输出中`
  for(int i = first; i <= last; i++) ans.push_back(p[i]);
  return ans;
}


// `cut with directed line A->B, return the left part`
// `may return a single point or a line segment`
Polygon CutPolygon(Polygon poly, Point A, Point B) {
  Polygon newpoly;
  int n = poly.size();
  for(int i = 0; i < n; i++) {
    Point C = poly[i];
    Point D = poly[(i+1)%n];
    if(dcmp(Cross(B-A, C-A)) >= 0) newpoly.push_back(C);
    if(dcmp(Cross(B-A, C-D)) != 0) {
      Point ip = GetLineIntersection(A, B-A, C, D-C);
      if(OnSegment(ip, C, D)) newpoly.push_back(ip);
    }
  }
  return newpoly;
}

// `点在圆心内。圆周上不算`
bool isInCircle(Point p, Point center, double R) {
  return dcmp(Length2(p-center) - R*R) < 0;
}

