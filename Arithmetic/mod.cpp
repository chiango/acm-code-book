typedef long long LL;

LL add_mod(LL a, LL b, LL n) {
	return (a+b) % n;
}
LL sub_mod(LL a, LL b, LL n) {
	return (a-b+n) % n;
}
LL mul_mod(LL a, LL b, LL n) {
	return (a*b) % n;
}
LL pow_mod(LL a, LL p) {
	if(p == 0) return 1;
	LL ans = pow_mod(a, p/2);
	ans = (long long)ans * ans % MOD;
	if(p%2) ans = (long long)ans * a % MOD;
	return ans;
}

void gcd(LL a, LL b, LL& d, LL& x, LL& y) {
	if(!b){ d = a; x = 1; y = 0; }
	else{ gcd(b, a%b, d, y, x); y -= x*(a/b); }
}

LL inv(LL a, LL n) {
	LL d, x, y;
	gcd(a, n, d, x, y);
	return d == 1 ? ( x + n ) % n : -1;
}

// n equations：x=a[i](mod m[i]) (0<=i<n)
LL china(int n, int* a, int *m) {
	LL M = 1, d, y, x = 0;
	for(int i = 0; i < n; i++) M *= m[i];
	for(int i = 0; i < n; i++) {
		LL w = M / m[i];
		gcd(m[i], w, d, d, y);
		x = (x + y*w*a[i]) % M;
	}
	return (x+M)%M;
}

//a^x == b (mod n).
int log_mod(int a, int b, int n) {
	int m, v, e = 1, i;
	m = (int) sqrt(n + 0.5);
	v = inv(pow_mod(a, m, n), n);
	map<int, int> x;
	x[1] = 0;
	for(int i = 1; i < m; ++i) {
		e = mul_mod(e, a, n);
		if(!x.count(e)) x[e] = i;
	}
	for(int i = 0; i < m; ++i) {
		if(x.count(b)) return i * m + x[b];
		b = mul_mod(b, v, n);
	}
	return -1;
}
