#define _cp(a,b) ((a)<(b))
typedef int elem_t;
elem_t kth_element(int n,elem_t* a,int k){
	elem_t t,key;
	int l=0,r=n-1,i,j;
	while (l<r){
		for (key=a[((i=l-1)+(j=r+1))>>1];i<j;){
			for (j--;_cp(key,a[j]);j--);
			for (i++;_cp(a[i],key);i++);
			if (i<j) t=a[i],a[i]=a[j],a[j]=t;
		}
		if (k>j) l=j+1;
		else r=j;
	}
	return a[k];
}

#include <cstdio>
int main() {
	int a[] = {1, 3, 5, 7, 9, 2, 4, 6, 8, 10};
	printf("%d\n", kth_element(10, a, 9));
	return 0;
}
