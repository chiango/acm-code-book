#include <cstdio>
typedef long long LL;
LL pow(LL a, LL b) {
	LL ret = 1, base = a;
	while(b) {
		if(b&1) ret *= base;
		base = base*base;
		b>>=1;
	}
	return ret;
}
int main() {
	LL a, b;
	while(scanf("%lld%lld", &a, &b) == 2) printf("%lld\n", pow(a,b));
	return 0;
}
